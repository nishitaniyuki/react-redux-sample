import React from 'react';

/* global LoadingImage */

const Loading = () =>
  <section className="post">
    <img
      className="loading"
      src={LoadingImage}
    />
  </section>;

export default Loading;
